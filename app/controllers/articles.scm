;; Controller articles definition of blog
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller articles) ; DO NOT REMOVE THIS LINE!!!

(use-modules (ice-9 format))

(get "/" (lambda (rc) (redirect-to rc "/articles/index")))
(get "/articles" (lambda (rc) (redirect-to rc "/articles/index")))

(get "/articles/index" #:conn #t
     (lambda (rc)
       (let* ((mtable (map-table-from-DB (:conn rc)))
	      (tbl (mtable 'get 'articles #:columns '(title body)))
	      (articles (map (lambda (x) (map (lambda (a) (cdr (assoc a x))) '("title" "body"))) tbl))
	      (tbl (format #f "<table>~%~:{<tr>~%<td>~s</td>~%<td>~s</td>~%</tr>~%~}</table>" articles)))
	 (view-render "index" (the-environment)))))

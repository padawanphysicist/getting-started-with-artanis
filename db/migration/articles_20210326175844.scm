;; Migration articles_20210326175844 definition of blog
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(create-artanis-migration articles_20210326175844) ; DO NOT REMOVE THIS LINE!!!

(use-modules (dbi dbi))

(migrate-create
  (let* ((conn (connect-db "sqlite3" "blog.db"))
	 (mtable (map-table-from-DB conn)))
    ;; Create table
    ((mtable 'create 'articles '((id integer)
				 (title varchar 100)
				 (body varchar 300)))
     'valid?)

    ;; Here I set some value to populate the table
    (mtable 'set 'articles #:id 1 #:title "Hello World" #:body "This is my first post.")
    (mtable 'set 'articles #:id 2 #:title "Hello (again) World" #:body "This is my second post."))
  (display "Created database blog.db\n"))

(migrate-up
  (create-table
    'articles
    '(id auto (#:primary-key))
    '(title char-field (#:not-null))
    '(body char-field (#:not-null)))
  (display "Add your up code\n"))
(migrate-down
  (drop-table 'articles)
  (display "Add your down code\n"))

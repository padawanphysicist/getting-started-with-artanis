# Getting Started with GNU Artanis

This is a very simple app with [GNU Artanis](https://www.gnu.org/software/artanis/).

It follows from sections 1 to 5 of the [starter guide of Rails](https://guides.rubyonrails.org/getting_started.html), which I believe were the most critical to have a functionall app with GNU Artanis, due to (until now) incomplete documentation.
